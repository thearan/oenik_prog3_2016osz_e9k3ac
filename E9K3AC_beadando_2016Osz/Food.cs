﻿namespace E9K3AC_beadando_2016Osz
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Begyűjthető kaják (színes random pontok) logikáját leíró osztály
    /// </summary>
    public class Food : MyShape
    {
        /// <summary>
        /// Alapértelmezett átmérő
        /// </summary>
        public const double StartDiameter = 10;

        /// <summary>
        /// Initializes a new instance of the <see cref="Food" /> class.
        /// Foodok mindíg random helyen és random színnel jönnek létre
        /// </summary>
        /// <param name="cx1">X canvaskoordináta randomizálásának alsó határa</param>
        /// <param name="cx2">X canvaskoordináta randomizálásának felső határa</param>
        /// <param name="cy1">Y canvaskoordináta randomizálásának alsó határa</param>
        /// <param name="cy2">Y canvaskoordináta randomizálásának felső határa</param>
        public Food(double cx1, double cx2, double cy1, double cy2) :
            base((double)Statics.R.Next((int)cx1, (int)cx2), (double)Statics.R.Next((int)cy1, (int)cy2), StartDiameter, Statics.RandomColor)
        {
            if (Foods == null)
            {
                Foods = new MyLittleHashTable(cx1, cx2, cy1, cy2, 50);
            }

            Foods.Add(this);
        }

        /// <summary>
        /// Gets: Összes eddig létrejött, még élő food-ok gyűjteményét tároló statikus hasítótáblát adja vissza
        /// </summary>
        public static MyLittleHashTable Foods 
        {
            get; private set; 
        }

        /// <summary>
        /// Aktuális példány kiveszi önmagát "törli" önmagát azzal, hogy kiveszi önmagát a statikus hasítótáblából.
        /// Ezáltal már nemlétezőnek tekintjük.
        /// </summary>
        protected override void Destruct()
        {
            Foods.Remove(this);
        }
    }
}
