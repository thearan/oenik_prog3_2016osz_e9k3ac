﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E9K3AC_beadando_2016Osz
{
    /// <summary>
    /// A labdák viselkedésének logikáját leíró osztály.
    /// </summary>
    public class Ball : MoveableShape
    {
        /// <summary>
        /// Minden labda kezdőátmérője (mely később még változik, mivel nől).
        /// </summary>
        private const double StartDiameter = 20;

        /// <summary>
        /// Referencia mely visszamutat arra a Ballgroup objektumra ami az aktuális objektumot tartalmazza.
        /// Elég ronda, de elkerülhetetlen, mivel minden labdának tudnia kell (legalább) azt , hogy hány elemű az a Labdacsoportban van.
        /// </summary>
        private BallGroup bg;

        /// <summary>
        /// Initializes a new instance of the <see cref="Ball"/> class.
        /// </summary>
        /// <param name="left">A labda köré írt négyzet balfelső sarkának x koordinátája</param>
        /// <param name="top">A labda köré írt négyzet balfelső sarkának y koordinátája</param>
        /// <param name="gropu"> Referencia mely visszamutat arra a Ballgroup objektumra ami az aktuális objektumot tartalmazza.
        /// Elég ronda, de elkerülhetetlen, mivel minden labdának tudnia kell (legalább) azt , hogy hány elemű az a Labdacsoportban van.</param>
        public Ball(double left, double top, BallGroup gropu)
            : base(left, top, StartDiameter, Statics.RandomColor)
        {
            if (Balls == null) 
            { 
                Balls = new List<Ball>(); 
            }

            Balls.Add(this);
            this.bg = gropu;
        }

        /// <summary>
        /// Gets: Egy listát ad vissza mely tartalmazza az összes ebből az osztályból létrejött objektumot
        /// </summary>
        public static List<Ball> Balls
        {
            get;
            private set;
        }

        /// <summary>
        /// Amortizáció: Meghíváskor bejárja a labdákat és
        /// kicsivel csökkenti minden labdaobjektum átmérőjét
        /// </summary>
        /// <param name="lb"> Át kell neki adni a labdagyüjteményt amin dolgozik</param>
        public static void Amortization(List<Ball> lb)
        {
            for (int i = 0; i < lb.Count; ++i)
            {
                if (lb[i].Diameter > Ball.StartDiameter)
                {
                    lb[i].Diameter -= 0.1;
                }
            }

            return;
        }

        /// <summary>
        /// Elmozgatja az objektumot a célja (ami a főlabda esetén az egér helye)
        /// irányába egy bizonyos mértékkel (ami viszont a labda átmérőjétől függ - minél nagyobb annál lassabb)
        /// </summary>
        /// <param name="reboundmode">reboundmode paraméter ha igaz, akkor az objektumok mozgatása
        /// a játéktér szélein úgy valósul meg hogy azok onnan visszapattannak.
        /// False érték esetén csak megállnak. Kevésbé néz ki jól:)</param>
        public override void Move(bool reboundmode)
        {
            this.Left += this.Dx / this.Diameter;
            this.Top += this.Dy / this.Diameter;
            this.ReboundMode(reboundmode); 
        }

        /// <summary>
        /// A labdánk egy kis kajacsomagot köp ki magából.
        /// </summary>
        /// <returns>A kiadott kajacsomag (W) objektum referenciája</returns>
        public W W()
        {
            if (this.Diameter <= StartDiameter)
            {
                return null;
            }

            W w = new W(this);
            this.Diameter = Math.Sqrt(Math.Pow(this.Diameter, 2) - Math.Pow(w.Diameter, 2));
            return w;
        }

        /// <summary>
        /// Azért felel, hogy mindíg amikor a labda megeszik egy másik objektumot, magába olvassza annak tömegét~területét.
        /// E labda tömege annyival nől.
        /// </summary>
        /// <param name="otherDiameter">A megkajálandó másik objektum átmérője</param>
        public void Eat(double otherDiameter)
        {
            this.Diameter = Math.Sqrt(Math.Pow(this.Diameter, 2) + Math.Pow(otherDiameter, 2));
        }

        /// <summary>
        /// Osztódást végez a labda enek hatására. 
        /// Feleakkora tömegűre csökkentjük a labdánkat majd létrehozunk a miénk alapján mégegy ugyanolyan labdát.
        /// </summary>
        /// <returns>Visszaadja a létrehozott labda referenciáját</returns>
        public Ball Split()
        {
            if (this.bg.Count >= 16 || this.Diameter <= StartDiameter)
            {
                return null;
            }

            this.Diameter /= Math.Sqrt(2);
            Ball newBall = new Ball(
                                    this.CenterX + (this.MomentumX * this.Radius) + (this.MomentumX * this.Diameter),
                                    this.CenterY + (this.MomentumY * this.Radius) + (this.MomentumY * this.Diameter), 
                                    this.bg);
            newBall.Diameter = this.Diameter;
            newBall.Color = this.Color;
            newBall.Dx = 2 * this.Dx;
            newBall.Dy = 2 * this.Dy;
            this.bg.Balls.Add(newBall);
            return newBall;
        }

        /// <summary>
        /// Felrobbantja a labdát, 
        /// addig hivogatja a split metódust amíg az már nem bontja tovább a labdát.
        /// </summary>
        public void Explosion()
        {
                Ball temp = this.Split();
                if (temp != null)
                {
                    temp.SetMomentum(
                        Statics.R.Next(0, (int)CanvasWitdh),
                        Statics.R.Next(0, (int)CanvasHeigth));
                    temp.Explosion();
                    this.Explosion();
                }
        }

        /// <summary>
        /// Kiszedi az aktuális objektumot az osztály statikus listájából, és a BallGroupból is.
        /// Ezzel nemlétezőnek tekintjük már az adott objektumot.
        /// </summary>
        protected override void Destruct()
        {
            Balls.Remove(this);
            this.bg.Balls.Remove(this);
        }
    }
}
