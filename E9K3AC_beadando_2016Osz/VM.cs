﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Shapes;

namespace E9K3AC_beadando_2016Osz
{
    /// <summary>
    /// ViewModel/Gamemodel osztály. Megjelenítésspecifikus dolgok.
    /// </summary>
    public class VM : Bindable
    {
        /// <summary>
        /// Hibák száma. Az a hiba amikor a játékos labdáit mind megette valamelyik másik.
        /// </summary>
        private int errors;

        /// <summary>
        /// A felhasználó által irányított fő Labdacsoportra mutató referencia.
        /// Alapvetően a GameLogicban tárolódik, de szükség volt ide is bemásolni, hogy a fő labdacsoport össztömege adatköthető legyen.
        /// </summary>
        private BallGroup mainBallgroupforBinding;

        /// <summary>
        /// Initializes a new instance of the <see cref="VM" /> class.
        /// </summary>
        /// <param name="c">A megjelenítésre használt canvas</param>
        public VM(Canvas c)
        {
            this.C = c;
            this.Zoom = 1;
            this.Errors = 0;
        }

        /// <summary>
        /// Gets: Referencia a megjelenítésre használt canvasra.
        /// </summary>
        public Canvas C { get; private set; }

        /// <summary>
        /// Gets or sets: A Canvasra való zoomolás aránya
        /// </summary>
        public double Zoom { get; set; }

        /// <summary>
        /// Gets or sets: Hibák száma
        /// </summary>
        public int Errors
        {
            get { return this.errors; }
            set { this.SetProperty(ref this.errors, value); }
        }

        /// <summary>
        /// Gets or sets: A felhasználó által irányított fő Labdacsoportra mutató referencia.
        /// </summary>
        public BallGroup MainBallgroupforBinding
        {
            get
            {
                return this.mainBallgroupforBinding;
            }

            set
            {
                this.SetProperty(ref this.mainBallgroupforBinding, value);
            }
        }

        /// <summary>
        /// Egy bemeneti MyShape objektumról eldönti hogy melyik leszármazott osztályba tartozik,
        /// és aszerint hívja meg a hozzá vizuális megjelenítést rendelő eljárást.
        /// </summary>
        /// <param name="myshape">MyShape példány amihez vizuális megjelenítést akarunk rendelni</param>
        public void BindingSwitcher(MyShape myshape)
        {
            if (myshape == null)
                {
                    return;
                }

            if (myshape is Food)
                { 
                    this.NewFood(myshape as Food); 
                }

            if (myshape is Ball)
                { 
                    this.NewBall(myshape as Ball); 
                }

            if (myshape is Virus)
                { 
                    this.NewVirus(myshape as Virus); 
                }

            if (myshape is W)
                { 
                    this.NewW(myshape as W); 
                }
        }

        /// <summary>
        /// Vizuális megjelenítést rendel egy Foodhoz a canvason.
        /// </summary>
        /// <param name="myFood">Food példány amit meg akarunk jeleníteni a canvason</param>
        private void NewFood(Food myFood)
        {
            if (myFood == null)
                {
                    return;
                }

            Ellipse myEllipse = new Ellipse();
            myEllipse.DataContext = myFood;
            myEllipse.Stroke = myFood.Color;
            myEllipse.Fill = myFood.Color;
            myEllipse.SetBinding(Ellipse.WidthProperty, new Binding("Diameter"));
            myEllipse.SetBinding(Ellipse.HeightProperty, new Binding("Diameter"));
            myEllipse.SetBinding(Canvas.LeftProperty, new Binding("Left"));
            myEllipse.SetBinding(Canvas.TopProperty, new Binding("Top"));
            this.C.Children.Add(myEllipse);
            MyShape.WaitingForBinding.Remove(myFood);
        }

        /// <summary>
        /// Vizuális megjelenítést rendel egy Ballhoz a canvason.
        /// </summary>
        /// <param name="myBall">Ball példány amit meg akarunk jeleníteni a canvason</param>
        private void NewBall(Ball myBall)
        {
            if (myBall == null)
                {
                    return;
                }

            Ellipse myEllipse = new Ellipse();
            myEllipse.DataContext = myBall;
            myEllipse.Stroke = new SolidColorBrush(new Color() { R = 0, G = 0, B = 0, A = 255 });
            myEllipse.Fill = myBall.Color;
            myEllipse.SetBinding(Ellipse.WidthProperty, new Binding("Diameter"));
            myEllipse.SetBinding(Ellipse.HeightProperty, new Binding("Diameter"));
            myEllipse.SetBinding(Canvas.LeftProperty, new Binding("Left"));
            myEllipse.SetBinding(Canvas.TopProperty, new Binding("Top"));
            this.C.Children.Add(myEllipse);
            MyShape.WaitingForBinding.Remove(myBall);
        }

        /// <summary>
        /// Vizuális megjelenítést rendel egy W-hez a canvason.
        /// </summary>
        /// <param name="myW">W példány amit meg akarunk jeleníteni a canvason</param>
        private void NewW(W myW)
        {
            if (myW == null)
            {
                return;
            }

            Ellipse myEllipse = new Ellipse();
            myEllipse.DataContext = myW;
            myEllipse.Stroke = myW.Color;
            myEllipse.Fill = myW.Color;
            myEllipse.SetBinding(Ellipse.WidthProperty, new Binding("Diameter"));
            myEllipse.SetBinding(Ellipse.HeightProperty, new Binding("Diameter"));
            myEllipse.SetBinding(Canvas.LeftProperty, new Binding("Left"));
            myEllipse.SetBinding(Canvas.TopProperty, new Binding("Top"));
            this.C.Children.Add(myEllipse);
            MyShape.WaitingForBinding.Remove(myW);
        }

        /// <summary>
        /// Vizuális megjelenítést rendel egy Virushoz a canvason.
        /// </summary>
        /// <param name="myVirus">Virus példány amit meg akarunk jeleníteni a canvason</param>
        private void NewVirus(Virus myVirus)
        {
            if (myVirus == null)
                {
                    return;
                }

            int n = 36;
            PointCollection polyPoints = new PointCollection();
            for (int i = 0; i < n; ++i)
            {
                double angle = i * 2 * Math.PI / n;
                Point p = new Point(myVirus.Radius * Math.Cos(angle), myVirus.Radius * Math.Sin(angle));
                if (i % 2 == 1)
                {
                    p.X *= 0.9;
                    p.Y *= 0.9;
                }

                p.X += myVirus.Radius;
                p.Y += myVirus.Radius;
                polyPoints.Add(p);
            }

            Polygon myPolygon = new Polygon();
            myPolygon.Fill = myVirus.Color;
            myPolygon.Stroke = Brushes.Black;
            myPolygon.DataContext = myVirus;
            myPolygon.SetBinding(Canvas.LeftProperty, new Binding("Left"));
            myPolygon.SetBinding(Canvas.TopProperty, new Binding("Top"));
            myPolygon.SetBinding(Polygon.WidthProperty, new Binding("Diameter"));
            myPolygon.SetBinding(Polygon.HeightProperty, new Binding("Diameter"));
            myVirus.PolyPoints = polyPoints;
            myPolygon.SetBinding(Polygon.PointsProperty, new Binding("PolyPoints"));
            this.C.Children.Add(myPolygon);
            MyShape.WaitingForBinding.Remove(myVirus);
        }
    }
}
