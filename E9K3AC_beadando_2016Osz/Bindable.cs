﻿namespace E9K3AC_beadando_2016Osz
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Adatköthető objektumok ősosztálya
    /// </summary>
    public class Bindable : INotifyPropertyChanged
    {
        /// <summary>
        /// ProtertyChanged esemény
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// PropertyChanged-et meghívó metódus
        /// </summary>
        /// <param name="name">Adatkötött tulajdonság neve</param>
        protected void OnPropertyChanged(string name = null)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null) 
            {
                handler(this, new PropertyChangedEventArgs(name)); 
            }
        }

        /// <summary>
        /// Tulajdonság frissítése
        /// </summary>
        /// <typeparam name="T">Tulajdonság típusa</typeparam>
        /// <param name="field">Tulajdonság referenciája</param>
        /// <param name="newvalue">Tulajdonság új értéke</param>
        /// <param name="name">Tulajdonság neve stringként</param>
        protected void SetProperty<T>(ref T field, T newvalue, [CallerMemberName] string name = null)
        {
            field = newvalue;
            this.OnPropertyChanged(name);
        }
    }
}
