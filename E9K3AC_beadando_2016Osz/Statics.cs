﻿namespace E9K3AC_beadando_2016Osz
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;

    /// <summary>
    /// Randomizációkat végző osztály
    /// </summary>
    public static class Statics
    {
        /// <summary>
        /// Gets or sets: Statikus radomizátor
        /// </summary>
        public static Random R 
            {
                get; 
                set; 
            }

        /// <summary>
        /// Gets: Randomszín generátor
        /// </summary>
        public static Color RandomColor
        {
            get 
            {
                Color c = new Color();
                c.R = (byte)Statics.R.Next(0, 255);
                c.G = (byte)Statics.R.Next(0, 255);
                c.B = (byte)Statics.R.Next(0, 255);
                c.A = 255;
                return c; 
            }
        }

        /// <summary>
        /// Osztály inicializáló
        /// </summary>
        public static void Init()
        {
            R = new Random();
        }
    }
}
