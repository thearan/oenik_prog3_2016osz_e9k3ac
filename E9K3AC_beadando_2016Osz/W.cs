﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E9K3AC_beadando_2016Osz
{
    /// <summary>
    /// W betű lenyomására a labda kis kajacsomagot bocsát ki magából.
    /// A W osztály eme kajacsomag logikáját írja le.
    /// </summary>
    public class W : MoveableShape
    {
        /// <summary>
        /// Egy kajacsomag átmérője
        /// </summary>
        private const double StartDiameter = 19;

        /// <summary>
        /// Initializes a new instance of the <see cref="W"/> class.
        /// Minden W objektumot Ball hoz létre, "köpi ki" magából. 
        /// Ezáltal a kajacsomag helye ás lendülete az őt létrehozó labda helyétől és lendületétől fog függeni.
        /// </summary>
        /// <param name="creator">A Ball objektum melyből a  lendületet, pozíciót, színt inicializáljuk</param>
        public W(Ball creator)
            : base(
            creator.CenterX + (creator.MomentumX * (creator.Radius + 10)) - (StartDiameter / 2), 
            creator.CenterY + (creator.MomentumY * (creator.Radius + 10)) - (StartDiameter / 2), 
            StartDiameter, 
            creator.Color.Color)
        {
            if (Ws == null)
            {
                Ws = new List<W>(); 
            }

            Ws.Add(this);

            this.MomentumX = 2 * creator.MomentumX;
            this.MomentumY = 2 * creator.MomentumY;
        }

        /// <summary>
        /// Gets: Egy listát ad vissza mely tartalmazza az összes ebből az osztályból létrejött objektumot
        /// </summary>
        public static List<W> Ws { get; private set; }

        /// <summary>
        /// Lendületének megfelelően arrébbmozgatja az objektumot.
        /// Csökkenti is egy kicsit a lendületet, mivel nem akarjuk hogy egy ilyen W objektum örökké összevissza ugráljon.
        /// </summary>
        /// <param name="reboundmode">reboundmode paraméter ha igaz, akkor az objektumok mozgatása
        /// a játéktér szélein úgy valósul meg hogy azok onnan visszapattannak.
        /// False érték esetén csak megállnak. Kevésbé néz ki jól:)</param>
        public override void Move(bool reboundmode)
        {
            base.Move(reboundmode);
            this.MomentumX *= 0.95;
            this.MomentumY *= 0.95;
        }

        /// <summary>
        /// Kiszedi az aktuális objektumot az osztály statikus listájából.
        /// </summary>
        protected override void Destruct()
        {
            Ws.Remove(this);
        }
    }
}
