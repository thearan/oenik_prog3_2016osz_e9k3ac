﻿namespace E9K3AC_beadando_2016Osz
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Navigation;
    using System.Windows.Shapes;
    using System.Windows.Threading;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Referencia a ViewModelre
        /// </summary>
        private VM vm;

        /// <summary>
        /// Referencia a GameLogicra
        /// </summary>
        private GameLogic gl;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow" /> class.
        /// </summary>
        public MainWindow() 
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Window_Loaded esemény; Az ablak inicializálása után lefutó kód. További inicializáció.
        /// </summary>
        /// <param name="sender">Esemény küldő</param>
        /// <param name="e">Esemény argumentumok</param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Statics.Init();
            this.vm = new VM((Content as Canvas).Children[0] as Canvas);
            this.DataContext = this.vm;
            this.gl = new GameLogic(this.vm);
        }

        /// <summary>
        /// Egérgörgetésre reagáló esemény.
        /// Canvas zoomolása
        /// </summary>
        /// <param name="sender">Esemény küldő</param>
        /// <param name="e">Esemény argumentumok</param>
        private void Canvas_MouseWheel(object sender, MouseWheelEventArgs e)
        {
                if (e.Delta > 0)
                {
                    this.vm.Zoom *= 1.1;
                }
                else
                {
                    this.vm.Zoom /= 1.1;
                }

                this.vm.C.LayoutTransform = new ScaleTransform(this.vm.Zoom, this.vm.Zoom);
        }

        /// <summary>
        /// Egérmozgásra reagáló esemény.
        /// A Főlabda mozgása célpontjának megváltoztatása
        /// </summary>
        /// <param name="sender">Esemény küldő</param>
        /// <param name="e">Esemény argumentumok</param>
        private void Canvas_MouseMove(object sender, MouseEventArgs e)
        {
            this.gl.MainBalls.MoveInto(e.GetPosition(this.vm.C).X, e.GetPosition(this.vm.C).Y);
        }

        /// <summary>
        /// Gomblenyomásra reagáló esemény.
        /// Space: osztódás
        /// W: kajacsomag köpés
        /// </summary>
        /// <param name="sender">Esemény küldő</param>
        /// <param name="e">Esemény argumentumok</param>
        private void Canvas_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.W)
            {
                this.gl.MainBalls.W();
            }

            if (e.Key == Key.Space)
            {
                this.gl.MainBalls.Split();
            }
        }
    }
}
