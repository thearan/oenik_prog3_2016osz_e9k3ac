﻿namespace E9K3AC_beadando_2016Osz
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;

    /// <summary>
    /// Minden játéktéren szereplő objektum absztrakt őse
    /// </summary>
    public abstract class MyShape : Bindable
    {
        /// <summary>
        /// Buffer, melybe a törlendő myshape objektumokat gyűjtjük.
        /// Azért van hogy amikor egy myshape[] gyűjteményt bejárunk 
        /// hogy megszüntessük a megszüntetendő objektumokat, kellemetlen lenne 
        /// ha akkor ki is törölnénk abból a gyűjteményből melyet épp bejárunk.
        /// Ezért bejárás közben csak kijelöljük (SelectForDestruct()) a törlendőket, és belerakjuk eme listába.
        /// és bejárás után megszűntetünk minden kijelöltet (DeleteAllSelectedShape()).
        /// </summary>
        private static List<MyShape> waitingForDestruct = new List<MyShape>();

        /// <summary>
        /// objektum bal széle
        /// </summary>
        private double left;

        /// <summary>
        /// objektum felső széle
        /// </summary>
        private double top;

        /// <summary>
        /// objektum átmérője
        /// </summary>
        private double diameter;

        /// <summary>
        /// objektum ajánlott színe. 
        /// Csak ajánlás, nem feltétlenül kell használni is megjelenítű szinten.
        /// </summary>
        private SolidColorBrush color;

        /// <summary>
        /// Initializes a new instance of the <see cref="MyShape" /> class.
        /// </summary>
        /// <param name="left">myshape objektum köré írt 4zet bal széle</param>
        /// <param name="top">myshape objektum köré írt 4zet jobb széle</param>
        /// <param name="diameter">myshape objektum átmérője</param>
        /// <param name="c">Ajánott szín</param>
        public MyShape(double left, double top, double diameter, Color c)
        {
            this.Left = left;
            this.Top = top;
            this.Diameter = diameter;
            this.color = new SolidColorBrush(c);

            if (WaitingForBinding == null)
            {
                WaitingForBinding = new List<MyShape>();
            }

            WaitingForBinding.Add(this);
        }

        /// <summary>
        /// Gets: az összes, aktuálisan még élő myshape objektum össztömegét tartja nyilván.
        /// </summary>
        public static double AllMass
        {
            get;
            private set;
        } // Összes objektum összetömegét tartja nyilván

        /// <summary>
        /// Gets or sets: Adatkötésre váró myshape objektumok listája. Létrehozáskor mindent berakunk ebbe a listába, 
        /// egy efeletti szinten pedig mindíg mikor az adott objektum megkapja az adatkötést, kivesszük ebből a listából.
        /// </summary>
        public static List<MyShape> WaitingForBinding
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets: objektum köré írt 4zet bal szélének koordinátája (x)
        /// </summary>
        public virtual double Left
        {
            get
            {
                return this.left;
            }

            set
            {
                this.SetProperty(ref this.left, value);
                this.OnPropertyChanged("CenterX");
            }
        }

        /// <summary>
        /// Gets or sets: objektum köré írt 4zet fölső szélének koordinátája (y)
        /// </summary>
        public virtual double Top
        {
            get
            {
                return this.top;
            }

            set
            {
                this.SetProperty(ref this.top, value);
                this.OnPropertyChanged("CenterY");
            }
        } 

        /// <summary>
        /// Gets or sets: objektum átmérője
        /// </summary>
        public double Diameter
        {
            get
            {
                return this.diameter;
            }

            set
            {
                AllMass += (Math.PI * Math.Pow(value / 2, 2)) - (Math.PI * Math.Pow(this.Diameter / 2, 2));
                this.SetProperty(ref this.diameter, value);
                this.OnPropertyChanged("Radius");
            }
        }

        /// <summary>
        /// Gets or sets: objektum ajánlott színe
        /// </summary>
        public SolidColorBrush Color
        {
            get
            {
                return this.color;
            }

            set 
            {
                this.SetProperty(ref this.color, value);
            }
        }

        /// <summary>
        /// Gets: Visszaadja az objektum közppontjának x koordinátáját
        /// </summary>
        public double CenterX 
        { 
            get 
            { 
                return this.Left + (this.Diameter / 2); 
            } 
        }

        /// <summary>
        /// Gets: Visszaadja az objektum közppontjának y koordinátáját
        /// </summary>
        public double CenterY 
        { 
            get 
            { 
                return this.Top + (this.Diameter / 2); 
            }
        } 

        /// <summary>
        /// Gets: Visszaadja az objektum sugarát
        /// </summary>
        public double Radius 
        { 
            get 
            { 
                return this.diameter / 2; 
            }
        }

        /// <summary>
        /// Egyesítjük a fedésben lévő objekumokat: nagyobbak egyék meg a kisebbeket (általában...)
        /// Elv:
        ///     A Virusok csak W-t esznek
        ///     A Ballok-ok mindent (Virus, W, Food, Ball) megesznek, de csak ha nagyobbak nála
        ///         Továbbá ha egy Ball virust eszik, felrobban.
        /// Tehát:
        ///     Bejárjuk a Ball-okat
        ///     {
        ///         Megetetünk vele minden food-ot ami vele fedésben van.
        ///         Egy belső ciklussal is bejárjuk a Ballok-at: Fedésben lévő ballokat találva a nagyobbal megetetjük a kisebbet.
        ///         Bejárjuk a Virusokat, ha a labda nagyobb a virusnál, megetetjük a virust a labdával, viszont a labdát felrobbanjuk sok kicsi labdára
        ///         Bejárjuk a W-ket, ha a egy fedésben lévő W kisebb a labdánál, a labda bekajálja
        ///     }
        ///     Bejárjuk a Virusokat
        ///     {
        ///         A virussal fedésben lévő W-ket bekajáltatjuk a virussal.
        ///     }
        /// </summary>
        /// <param name="ballcollection">Labdák/ball-ok azon listája amellyel dolgozunk</param>
        /// <param name="foodcollection">Foodok/kaják azon listája amellyel dolgozunk</param>
        /// <param name="viruscollection">Virusok azon listája amellyel dolgozunk</param>
        /// <param name="wcollection">W-k azon listája amellyel dolgozunk</param>
        public static void UniteCoveringShapes(
                                                List<Ball> ballcollection, 
                                                MyLittleHashTable foodcollection, 
                                                List<Virus> viruscollection, 
                                                List<W> wcollection) 
        {
            for (int i = 0; i < ballcollection.Count; ++i)
            {
                if (foodcollection != null && foodcollection.Count != 0)
                {
                    MyShape[] temp = foodcollection.Search((int)ballcollection[i].Left, (int)ballcollection[i].top, (int)ballcollection[i].diameter);
                    for (int j = 0; j < temp.Length; ++j)
                    {
                        if (ballcollection[i].Distance(temp[j]) < 0)
                        {
                            ballcollection[i].Diameter = Math.Sqrt(Math.Pow(temp[j].Diameter, 2) + Math.Pow(ballcollection[i].Diameter, 2));
                            (temp[j] as Food).SelectForDestruct();
                        }
                    }
                }

                for (int j = i + 1; j < ballcollection.Count; ++j)
                {
                    if (ballcollection[i].Distance(ballcollection[j]) < 0)
                    {
                        if (ballcollection[i].Diameter > ballcollection[j].Diameter)
                        {
                            ballcollection[i].Eat(ballcollection[j].Diameter);
                            ballcollection[j].SelectForDestruct();
                        }

                        if (ballcollection[i].Diameter < ballcollection[j].Diameter)
                        {
                            ballcollection[j].Eat(ballcollection[i].Diameter);
                            ballcollection[i].SelectForDestruct();
                        }
                    }
                }

                for (int j = 0; j < viruscollection.Count; ++j)
                {
                    if (ballcollection[i].Distance(viruscollection[j]) < 0 &&
                        ballcollection[i].Diameter > viruscollection[j].Diameter)
                    {
                        ballcollection[i].Eat(viruscollection[j].Diameter);
                        viruscollection[j].SelectForDestruct();

                        ballcollection[i].Explosion();
                    }
                }

                if (wcollection != null)
                {
                    for (int j = 0; j < wcollection.Count; ++j)
                    {
                        if (ballcollection[i].Distance(wcollection[j]) < 0 && ballcollection[i].Diameter > wcollection[j].Diameter)
                        {
                            ballcollection[i].Eat(wcollection[j].Diameter);
                            wcollection[j].SelectForDestruct();
                        }
                    }
                }
            }

            for (int i = 0; i < viruscollection.Count; ++i)
            {
                if (wcollection != null)
                {
                    for (int j = 0; j < wcollection.Count; ++j)
                    {
                        if (viruscollection[i].Distance(wcollection[j]) < 0 && viruscollection[i].Diameter > wcollection[j].Diameter)
                        {
                            viruscollection[i].Eat(wcollection[j]);
                            wcollection[j].SelectForDestruct();
                        }
                    }
                }
            }

            DeleteAllSelectedShape();
        }

        /// <summary>
        /// Két objektum távolságát számítja ki. 
        /// Távolság alatt itt azt kell érteni, hogy a kisebb középpontja milyen messze van a nagyobb szélétől.
        /// Magyarán a távolság akkor 0 ha a nagyobb objektum körvonala eléri a kisebb középpontját, ekkor (vagy ennél kisebb távolságnál) tekintem úgy hogy fedésben vannak.
        /// </summary>
        /// <param name="masik">Az összehasonlítandó 2 objektum közül az az egyik akinek meghívjuk ezt a metódusát, a másikat itt adjuk be paraméterként</param>
        /// <returns>A kettő közti távolságot adja vissza a fenti szemlélet alapján</returns>
        public double Distance(MyShape masik)
        { 
            return 
                Math.Sqrt(Math.Pow(this.CenterX - masik.CenterX, 2) + 
                Math.Pow(this.CenterY - masik.CenterY, 2)) - 
                Math.Max(this.Radius, masik.Radius); 
        }

        /// <summary>
        /// Objektum ezáltal törli önmagát a statikus gyűjteményekből melyekbe eltároltuk (melynek célja az összes még élő objektum nyilvántartása). 
        /// Ezek a gyűjtemények (legalábis arra a célra) csak a leszármazottakban léteznek, ezér lett ez absztrakt.
        /// </summary>
        protected abstract void Destruct();

        /// <summary>
        /// Az összes törlésre kijelölt objektumot törli
        /// </summary>
        private static void DeleteAllSelectedShape()
        {
            for (int i = 0; i < waitingForDestruct.Count; ++i)
            {
                waitingForDestruct[i].Destruct();
            }

            waitingForDestruct.Clear();
        }

        /// <summary>
        /// Kijelöli törlésre az objektumot.
        /// </summary>
        private void SelectForDestruct()
        {
            this.Diameter = 0;
            waitingForDestruct.Add(this);
        }
    }
}
