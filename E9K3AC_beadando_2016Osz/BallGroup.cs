﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E9K3AC_beadando_2016Osz
{
    /// <summary>
    /// Együtt mozgatható labdacsoportokat leító osztály
    /// </summary>
    public class BallGroup : Bindable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BallGroup" /> class.
        /// </summary>
        /// <param name="startball_left">Ballgroup bal szélének (x) koordinátája a canvason</param>
        /// <param name="startball_top">Ballgroup felső szélének (y) koordinátája a canvason</param>
        public BallGroup(double startball_left, double startball_top)
        {
            this.Balls = new List<Ball>();
            this.Balls.Add(new Ball(startball_left, startball_top, this));
        }

        /// <summary>
        /// Gets: Ballgroup által egybefogott Ballok listáját adja
        /// </summary>
        public List<Ball> Balls 
        {
            get; private set; 
        }

        /// <summary>
        /// Gets: Ballgroup által egybefogott Ballok számát adja vissza
        /// </summary>
        public int Count 
        {
            get
            {
                return this.Balls.Count;
            }
        }

        /// <summary>
        /// Gets: Legnagyobb labda átmérőjét adja vissza
        /// </summary>
        public double MaxDiameter
        {
            get
            {
                double max = 0;
                for (int i = 0; i < this.Count; ++i)
                {
                    double temp = this.Balls[i].Diameter;
                    if (temp > max)
                    {
                        max = this.Balls[i].Diameter;
                    }
                }

                return max;
            }
        }

        /// <summary>
        /// Gets: egy a Ballgroup által egybefogott Ballok területével arányos mérőszámot ad vissza
        /// </summary>
        public double SumDiameter
        {
            get
            {
                double sum = 0;
                for (int i = 0; i < this.Count; ++i)
                {
                    sum += Math.Pow(this.Balls[i].Diameter, 2);
                }

                return (double)(int)Math.Sqrt(sum);
            }
        }

        /// <summary>
        /// Módosítja a labdacsoport pillanatnyi irányát a paraméterként megadott (distanceX, distanceY) pont irányába.
        /// Belül, az eljárásban a tempx s tempy változók használata arra ügyel, hogy meg legyen nehezítve a labdák azonnali egyesülése.
        /// </summary>
        /// <param name="distanceX">Ballgroup mozgása célpontjának x koordinátája</param>
        /// <param name="distanceY">Ballgroup mozgása célpontjának y koordinátája</param>
        public void MoveInto(double distanceX, double distanceY)
        {
            for (int i = 0; i < this.Count; ++i)
            {
                double tempx = 0;
                double tempy = 0;
                for (int j = 0; j < this.Count; ++j)
                {
                    double tempdouble = Math.Max(this.Balls[i].Radius, this.Balls[j].Radius);
                    double tempdouble2 = this.Balls[i].Distance(this.Balls[j]);
                    if (tempdouble2 < tempdouble)
                    {
                        tempx += this.Balls[i].Left - this.Balls[j].Left;
                        tempy += this.Balls[i].Top - this.Balls[j].Top;
                    }
                }

                this.Balls[i].SetMomentum(distanceX + tempx, distanceY + tempy);
            }

            this.OnPropertyChanged("sumDiameter");
        }

        /// <summary>
        /// Meghívja az összes tartalmazott labda Split() metódusát, tehát osztódásra kényszeríti őket.
        /// </summary>
        public void Split()
        {
            int count = this.Count;
            for (int i = 0; i < count; ++i)
            {
                Ball temp = this.Balls[i].Split();
            }
        }

        /// <summary>
        /// Meghívja az összes tartalmazott labda W() metódusát, tehát kajacsomag adásra kényszeríti őket.
        /// </summary>
        public void W()
        {
            for (int i = 0; i < this.Balls.Count; ++i)
            {
                this.Balls[i].W();
            }
        }
    }
}
