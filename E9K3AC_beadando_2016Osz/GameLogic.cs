﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace E9K3AC_beadando_2016Osz
{
    /// <summary>
    /// A játék logikáját, egy adott pillanatban való működését leíró osztály
    /// </summary>
    public class GameLogic
    {
        /// <summary>
        /// Referencia a Viewmodel/Gamemodelre
        /// </summary>
        private VM vm;

        /// <summary>
        /// A játékban résztvevő Labdacsoportok
        /// </summary>
        private List<BallGroup> ballGroups;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogic" /> class.
        /// </summary>
        /// <param name="vm">Paraméterként megkapja az aktuális ViewModelt amivel dolgozunk</param>
        public GameLogic(VM vm)
        {
            this.vm = vm;
            this.MainBalls = new BallGroup(0, 0);
            this.ballGroups = new List<BallGroup>();
            this.vm.MainBallgroupforBinding = this.MainBalls;
            MoveableShape.CanvasHeigth = vm.C.ActualHeight;
            MoveableShape.CanvasWitdh = vm.C.ActualWidth;
            for (int i = 0; i < 5; ++i)
            {
                BallGroup tempball = new BallGroup(Statics.R.Next(0, (int)this.vm.C.ActualWidth), Statics.R.Next(0, (int)this.vm.C.ActualHeight));
                this.ballGroups.Add(tempball);
                tempball.MoveInto(Statics.R.Next(0, (int)this.vm.C.ActualWidth), Statics.R.Next(0, (int)this.vm.C.ActualHeight));
                new Virus(Statics.R.Next(0, (int)vm.C.ActualWidth), Statics.R.Next(0, (int)vm.C.ActualHeight));
            }

            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(10);
            timer.Tick += this.Timer_Tick;
            timer.Start();
        }

        /// <summary>
        /// Gets: A fő Labdacsoport amit a felhasználó irányít
        /// </summary>
        public BallGroup MainBalls { get; private set; }

        /// <summary>
        /// A játék egy Timerperiódusát leíró metódus
        /// </summary>
        /// <param name="sender">Esemény Küldő</param>
        /// <param name="e">Esemény Argumentumok</param>
        private void Timer_Tick(object sender, EventArgs e)
        {
            for (int i = 0; i < MyShape.WaitingForBinding.Count; ++i)
                {
                    this.vm.BindingSwitcher(MyShape.WaitingForBinding[i]);
                    --i;
                }

            if (MyShape.AllMass < 1000000)
                {
                    new Food(0, this.vm.C.ActualWidth, 0, this.vm.C.ActualHeight);
                }
            else
                {
                    Ball.Amortization(Ball.Balls);
                }

            if (Virus.Viruses.Count < 20)
                {
                    new Virus(Statics.R.Next(0, (int)this.vm.C.ActualWidth), Statics.R.Next(0, (int)this.vm.C.ActualHeight));
                }

            foreach (MoveableShape m in MoveableShape.MoveableShapes) 
                {
                    m.Move(true); 
                }

            MyShape.UniteCoveringShapes(Ball.Balls, Food.Foods, Virus.Viruses, W.Ws);

            for (int i = 0; i < this.vm.C.Children.Count; i++)
            {
                if ((this.vm.C.Children[i] as FrameworkElement).Width <= 0)
                {
                    this.vm.C.Children.RemoveAt(i);
                }
            }

            if (this.MainBalls.Count < 1 || this.MainBalls.SumDiameter <= 0) 
            {
                MessageBox.Show("Vesztettél");
                this.MainBalls = new BallGroup(0, 0);
                this.vm.MainBallgroupforBinding = this.MainBalls;
                this.vm.Errors++;
            }

            if (Ball.Balls.Count - this.MainBalls.Count < 6)
            {
                BallGroup tempball = new BallGroup(Statics.R.Next(0, (int)this.vm.C.ActualWidth), Statics.R.Next(0, (int)this.vm.C.ActualHeight));
                this.ballGroups.Add(tempball);
                tempball.MoveInto(Statics.R.Next(0, (int)this.vm.C.ActualWidth), Statics.R.Next(0, (int)this.vm.C.ActualHeight));
            }
        }
    }
}
