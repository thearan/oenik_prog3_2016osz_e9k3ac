﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E9K3AC_beadando_2016Osz
{
    /// <summary>
    /// Mozogni képes objektumok logikáját leíró osztály
    /// </summary>
    public abstract class MoveableShape : MyShape
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MoveableShape" /> class.
        /// </summary>
        /// <param name="left">Pozíció bal széle a canvason</param>
        /// <param name="top">Pozíció jobb széle a canvason</param>
        /// <param name="diameter">Objektum átmérője</param>
        /// <param name="c">Objektum színe</param>
        public MoveableShape(double left, double top, double diameter, System.Windows.Media.Color c)
            : base(left, top, diameter, c)
        {
        }

        /// <summary>
        /// Gets: Visszaad egy az összes MoveableShape típusú objektumot tartalmazó listát.
        /// (leszármazott osztályok statikus listáiból gereblyézi össze, valós saját adatmező nincs mögötte)
        /// </summary>
        public static IEnumerable<MoveableShape> MoveableShapes
        {
            get 
            {
                IEnumerable<MoveableShape> temp = Virus.Viruses.Union<MoveableShape>(Ball.Balls);
                if (W.Ws != null)
                {
                    return W.Ws.Union<MoveableShape>(temp);
                }
                else
                {
                    return temp;
                }
            }
        }

        /// <summary>
        /// Gets or sets: A Canvas magassága. Mozgatható logikáknál azért fontos ezt tudni, hogy ne mozoghassanak ki a canvasról.
        /// </summary>
        public static double CanvasHeigth { get; set; }

        /// <summary>
        /// Gets or sets: A Canvas szélessége. Mozgatható logikáknál azért fontos ezt tudni, hogy ne mozoghassanak ki a canvasról.
        /// </summary>
        public static double CanvasWitdh { get; set; }

        /// <summary>
        /// Gets: visszaadja az adott objektum bal szélének koordinátáját.
        /// Az overrideolás azt hivatott biztosítani hogy a canvas széléhez érkező objektum megálljon.
        /// </summary>
        public override double Left
        {
            get
            {
                return base.Left;
            }

            set
            {
                if (value < -this.Radius)
                {
                    base.Left = -this.Radius;
                }
                else if (value > CanvasWitdh - this.Radius)
                {
                    base.Left = CanvasWitdh - this.Radius;
                }
                else
                {
                    base.Left = value;
                }
            }
        }

        /// <summary>
        /// Gets: visszaadja az adott objektum felső szélének koordinátáját.
        /// Az overrideolás azt hivatott biztosítani hogy a canvas széléhez érkező objektum megálljon.
        /// </summary>
        public override double Top
        {
            get
            {
                return base.Top;
            }

            set
            {
                if (value < -this.Radius)
                {
                    base.Top = -this.Radius;
                }
                else if (value > CanvasHeigth - this.Radius)
                {
                    base.Top = CanvasHeigth - this.Radius;
                }
                else
                {
                    base.Top = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets: Az objektum lendületirányának x komponense. Normalizált, egységnyi hosszúságú vektor.
        /// </summary>
        public double MomentumX { get; protected set; }

        /// <summary>
        /// Gets or sets: Az objektum lendületirányának y komponense. Normalizált, egységnyi hosszúságú vektor.
        /// </summary>
        public double MomentumY { get; protected set; }

        /// <summary>
        /// Gets or sets: Az objektum lendületnek x komponense.
        /// </summary>
        protected double Dx { get; set; }

        /// <summary>
        /// Gets or sets: Az objektum lendületnek y komponense.
        /// </summary>
        protected double Dy { get; set; }

        /// <summary>
        /// Az objektum valós mozgatását ez a metódus valósítja meg.
        /// </summary>
        /// <param name="reboundmode">Abban a kivételes esetben ha a canvas szélére ér visszapattanjon-e az objektum (true) vagy csak álljon meg (false)?</param>
        public virtual void Move(bool reboundmode)
        {
            this.Left += 200 * this.MomentumX / this.Diameter;
            this.Top += 200 * this.MomentumY / this.Diameter;
            this.ReboundMode(reboundmode);
        }

        /// <summary>
        /// Beállítja az objektum aktuális lendületvektorát (Dx, Dy) és annak irányát külön nyilvántartó változókat (MomentumX, MomentumY)
        /// </summary>
        /// <param name="distanceX">x koordinátája annak a pontnak a canvason, melybe szeretnénk hogy haladjon az objektum</param>
        /// <param name="distanceY">y koordinátája annak a pontnak a canvason, melybe szeretnénk hogy haladjon az objektum</param>
        public void SetMomentum(double distanceX, double distanceY)
        {
            this.Dx = distanceX - this.CenterX;
            this.Dy = distanceY - this.CenterY;
            this.MomentumX = this.Dx / Math.Sqrt(Math.Pow(this.Dx, 2) + Math.Pow(this.Dy, 2));
            this.MomentumY = this.Dy / Math.Sqrt(Math.Pow(this.Dx, 2) + Math.Pow(this.Dy, 2));
        }

        /// <summary>
        /// Azesetben ha a játéktér szélére ér az objektum onnan visszapattan, és a játktér közepe felé feszi irányát.
        /// </summary>
        /// <param name="reboundmode">Engedélyező paraméter. Csak true érték esetén hajtódik végre a metódus</param>
        protected void ReboundMode(bool reboundmode)
        {
            if (reboundmode &&
                (this.CenterX <= 0 ||
                this.CenterY <= 0 ||
                this.CenterX >= CanvasWitdh ||
                this.CenterY >= CanvasHeigth))
            {
                this.SetMomentum(CanvasWitdh / 2, CanvasHeigth / 2);
            }
        }
    }
}
