﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace E9K3AC_beadando_2016Osz
{
    /// <summary>
    /// Virusok logikáját leíró osztály. (Virusok arra vannak, hogy ha egy labda rájuk megy, felrobbantsák).
    /// </summary>
    public class Virus : MoveableShape
    {
        /// <summary>
        /// Minden virus átmérője létrehozáskor
        /// </summary>
        private const double StartDiameter = 50;

        /// <summary>
        /// A Virus ajánlott színe
        /// </summary>
        private static Color recommendedColor = new System.Windows.Media.Color() { R = 0, G = 255, B = 0, A = 255 };

        /// <summary>
        /// Initializes a new instance of the <see cref="Virus" /> class.
        /// </summary>
        /// <param name="x">Virus köré írt 4zet bal szélének koordinátája</param>
        /// <param name="y">Virus köré írt 4zet felső szélének koordinátája</param>
        public Virus(double x, double y)
            : base(x, y, StartDiameter, recommendedColor)
        {
            if (Viruses == null)
            {
                Viruses = new List<Virus>();
            }

            Viruses.Add(this);
            this.PolyPoints = new PointCollection();
        }

        /// <summary>
        /// Gets: Az összes még élő Virus objektumot nyilvántartó statikus lista
        /// </summary>
        public static List<Virus> Viruses
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets: A Virusokat jelen verzióban a vizuális szinten poligonokkal rendeljük össze. 
        /// Ehhez nyújt támogatást ez a változó a logikai szinten, 
        /// amennyiben így akarjuk ábrázolni, de nem kötelező.
        /// </summary>
        public PointCollection PolyPoints 
        {
            get; 
            set; 
        }

        /// <summary>
        /// Virus mozgatása abban az esetben amikor w hatására klónozza magát a virus.
        /// Ekkor azt szeretnénk, hogy "kirepüljön" a korábbi helyéről az utód-virus, de valahol álljon meg.
        /// Emiatt felülírtam a mozgatást megvalósító metúdust, hogy minden timerütemre csökkenjen a virus sebessége.
        /// </summary>
        /// <param name="reboundmode">Visszapattanjon-e a virus ha a pálya szélére ér?</param>
        public override void Move(bool reboundmode)
        {
            base.Move(reboundmode);
            this.MomentumX *= 0.95;
            this.MomentumY *= 0.95;
        }

        /// <summary>
        /// Ha Virus fedésbe kerül egy w-vel, megeszi (tömegét saját magához adja), és ha bizonyos határ fölé hízott, klónozza magát.
        /// </summary>
        /// <param name="w">A megevendő W objektum</param>
        public void Eat(W w)
        {
            if (this.Diameter >= Math.Sqrt(2) * StartDiameter)
            {
                this.ModifyDiameterandPolyPoints(-this.Diameter / Math.Sqrt(2));
                this.Split(w);
            }

            this.ModifyDiameterandPolyPoints(w.Diameter);
        }

        /// <summary>
        /// "Törli" magát az objektum, kiveszi az aktuális példányt a statikus listából, ezzel megszűntnek tekintjük.
        /// </summary>
        protected override void Destruct()
        {
            Viruses.Remove(this);
        }

        /// <summary>
        /// Klónozza magát a virus/osztódik. Ez mindig annak hatására történik hogy belelövünk egy w-t.
        /// </summary>
        /// <param name="inductor">Osztódást kiváltó W kajacsomag</param>
        /// <returns>Biztonság kedvéért visszaadjuk a klón Virus referenciáját, hátha kellhet valamire</returns>
        private Virus Split(W inductor)
        {
            Virus newVirus = new Virus(
                                       this.Left + (inductor.MomentumX * this.Diameter),
                                       this.Top + (inductor.MomentumY * this.Diameter));
            newVirus.Diameter = this.Diameter;
            newVirus.Color = this.Color;
            newVirus.MomentumX = 2 * inductor.MomentumX;
            newVirus.MomentumY = 2 * inductor.MomentumY;
            return newVirus;
        }

        /// <summary>
        /// Ha egy virus W-t eszik, módosul a mérete: mind az átmérője, mind az őt reprezentáló poligon mérete. 
        /// A kettőnek együtt kell változni, ezt biztosítja ez a metódus
        /// </summary>
        /// <param name="diameterchange">Ama objektum átmérője amelyet be akarunk olvasztani (+) vagy le akarunk hasítani (-) a virusról.</param>
        private void ModifyDiameterandPolyPoints(double diameterchange)
        {
            double newDiameter = Math.Sqrt(Math.Pow(this.Diameter, 2) + (Math.Pow(diameterchange, 2) * Math.Sign(diameterchange)));
            PointCollection polypoints2 = new PointCollection(this.PolyPoints);
            for (int i = 0; i < polypoints2.Count; ++i)
            {
                polypoints2[i] = new Point(newDiameter * polypoints2[i].X / this.Diameter, newDiameter * polypoints2[i].Y / this.Diameter);
            }

            this.PolyPoints = polypoints2;
            this.Diameter = newDiameter;
            this.OnPropertyChanged("PolyPoints");
        }
    }
}
