﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E9K3AC_beadando_2016Osz
{
    /// <summary>
    /// Saját hasítótábla a Foodok tárolásához. 
    /// Azért indokolt mert rettentő sok lehet belőlük a játéktéren, viszont 
    /// maximális a diverzitás köztük a koordinátáik tekintetében, mivel azok random jönnek létre.
    /// A játékteret/ canvast felosztjuk (a dividion paraméter által meghatározott nagyságú) cellákra, 
    /// ezekhez rendeljük a koordinátáik alapján a food objektumokat
    /// </summary>
    public class MyLittleHashTable
    {
        /// <summary>
        /// Kétdimenziós tömb, melynek minden cellájában Lista. Ebben fogjuk tárolni a Foodokat.
        /// </summary>
        private List<Food>[,] table;

        /// <summary>
        /// A játéktér lehetséges legkisebb x koordinátája
        /// </summary>
        private double minX;

        /// <summary>
        /// A játéktér lehetséges legkisebb y koordinátája
        /// </summary>
        private double minY;

        /// <summary>
        /// A játéktér felosztása: Hány pixel oldalhosszúságú legyen az a négyzet, melynek területére kerülő foodokat közös listában tároljuk.
        /// </summary>
        private double division;

        /// <summary>
        /// Hasítótábla által tartalmazott elemek száma.
        /// </summary>
        private int count;

        /// <summary>
        /// Initializes a new instance of the <see cref="MyLittleHashTable" /> class.
        /// </summary>
        /// <param name="minX">A játéktér lehetséges legkisebb x koordinátája</param>
        /// <param name="maxX">A játéktér lehetséges legnagyobb x koordinátája</param>
        /// <param name="minY">A játéktér lehetséges legkisebb y koordinátája</param>
        /// <param name="maxY">A játéktér lehetséges legnagyobb y koordinátája</param>
        /// <param name="division">A játéktér felosztása: Hány pixel oldalhosszúságú legyen az a négyzet, melynek területére kerülő foodokat közös listában tároljuk.</param>
        public MyLittleHashTable(double minX, double maxX, double minY, double maxY, double division)
        {
            this.minX = minX;
            this.minY = minY;
            this.division = division;
            int width = (int)((maxX - minX) / division) + 1;
            int heigth = (int)((maxY - minY) / division) + 1;
            this.table = new List<Food>[width, heigth];
            for (int i = 0; i < width; ++i)
            {
                for (int j = 0; j < heigth; ++j)
                {
                    this.table[i, j] = new List<Food>();
                }
            }
        }

        /// <summary>
        /// Gets: Hasítótábla által tartalmazott elemek száma.
        /// </summary>
        public int Count
        {
            get 
            {
                return this.count; 
            }
        }

        /// <summary>
        /// Elhelyez a hasítótáblában egy új elemet a neki való cellába.
        /// </summary>
        /// <param name="myshape">Elhelyezendő elem</param>
        public void Add(Food myshape)
        {
            int[] idx = this.Function(myshape.Left, myshape.Top);
            this.table[idx[0], idx[1]].Add(myshape);
            this.count++;
        }

        /// <summary>
        /// Kiveszi a hasítótáblából a kiveendő elemet. Koordinátái alapján azonosítja be, hogy melyik cellából kell kivenni.
        /// </summary>
        /// <param name="myshape">Kiveendő elem</param>
        public void Remove(Food myshape)
        {
            int[] idx = this.Function(myshape.Left, myshape.Top);
            if (this.table[idx[0], idx[1]].Remove(myshape) == true)
            {
                this.count--; 
            }
        }

        /// <summary>
        /// Megkeresi a hasítótáblában azokat a Food elemeket melyeket egy adott left, top, és diameter paraméterekkel
        /// jellemezhető myshape objektum (Ball) köré rajzolható négyzet lefed.
        /// </summary>
        /// <param name="left">Ball objektum köré írt négyzet bal széle</param>
        /// <param name="top">Ball objektum köré írt négyzet felső széle</param>
        /// <param name="diameter">Ball objektum átmérője = négyzet oldalhossza</param>
        /// <returns>Visszaadja az elképzelt négyzet alá eső cellák által tartalmazott összes food-ot</returns>
        public Food[] Search(double left, double top, double diameter)
        {
            double right = left + diameter + this.division;
            double bottom = top + diameter + this.division;

            int[] edge1 = this.Function(left, top);
            if (edge1[0] < 0)
                { 
                    edge1[0] = 0; 
                }

            if (edge1[1] < 0) 
                { 
                    edge1[1] = 0; 
                }

            if (edge1[0] >= this.table.GetLength(0)) 
                { 
                    edge1[0] = this.table.GetLength(0) - 1; 
                }

            if (edge1[1] >= this.table.GetLength(1))
                { 
                    edge1[1] = this.table.GetLength(1) - 1; 
                }

            int[] edge2 = this.Function(right, bottom);
            if (edge2[0] < 0) 
                { 
                    edge2[0] = 0; 
                }

            if (edge2[1] < 0) 
                { 
                    edge2[1] = 0; 
                }

            if (edge2[0] >= this.table.GetLength(0)) 
                { 
                    edge2[0] = this.table.GetLength(0) - 1; 
                }

            if (edge2[1] >= this.table.GetLength(1)) 
                { 
                    edge2[1] = this.table.GetLength(1) - 1; 
                }

            int estimatedLength = 0;
            for (int i = edge1[0]; i <= edge2[0]; ++i)
            {
                for (int j = edge1[1]; j <= edge2[1]; ++j)
                {
                    estimatedLength += this.table[i, j].Count;
                }
            }

            Food[] temp = new Food[estimatedLength];

            int k = 0;
            for (int i = edge1[0]; i <= edge2[0]; ++i)
            {
                for (int j = edge1[1]; j <= edge2[1]; ++j)
                {
                    foreach (Food ms in this.table[i, j])
                    {
                        temp[k] = ms; 
                        ++k;
                    }
                }
            }

            return temp;
        }

        /// <summary>
        /// A hasító függvény. Megadja a food koordinátái alapján, hogy melyik cellába kell betenni.
        /// </summary>
        /// <param name="x">food objektum left koordinátája</param>
        /// <param name="y">food objektum top koordinátája</param>
        /// <returns>Kételemű int tömböt ad vissza, melyek a cellakoordinátái a food leendő helyének
        /// pl.: int[] idx = this.Function(myshape.Left, myshape.Top);
        ///      this.table[idx[0], idx[1]].Add(myshape);
        /// </returns>
        private int[] Function(double x, double y)
        {
            int idxX = (int)((x - this.minX) / this.division);
            int idxY = (int)((y - this.minY) / this.division);
            return new int[] { idxX, idxY };
        }
    }
}
